# Maze solving algorithm for Gentrack application

This code employs a recursive South East biased path finding algorithm. The solution (if it exists) is then
printed to the terminal. It is implemented as a class.

There are two .java files. One is test and the other is path. test.java is the driver program containing 
the main function that just runs the following;

```java
path p = new path("large_input.txt");
```

The input parameter is a string. Change this to the name of the text file containing the maze you want to test.

path.java contains the class that solves the maze. The code flow is as follows;

```
Import maze --> solve maze --> print maze
```

The class is split into three functions. One for importing the text file, one for solving the maze and another
for printing the maze. It was separated like this to keep the code clean and readable.