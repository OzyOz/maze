package maze_solver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class path
{
	private char[] data;
	private String[] values = new String[2];
	private int col, row;
	private int start_x, start_y;
	private int end_x, end_y;
	private char[][] maze;
	
	public path(String file)
	{
		ImportData(file);
		
		if (FindPath(start_x, start_y))
			WriteMaze();
		else
			System.out.print("There is no path from Start to End");
	}
	
	private void ImportData(String filename)
	{
		
		try
		{
			values = Files.readAllLines(Paths.get(filename)).get(0).split(" ");
			col = Integer.valueOf(values[0]);
			row = Integer.valueOf(values[1]);
			
			values = Files.readAllLines(Paths.get(filename)).get(1).split(" ");
			start_x = Integer.valueOf(values[0]);
			start_y = Integer.valueOf(values[1]);
			
			values = Files.readAllLines(Paths.get(filename)).get(2).split(" ");
			end_x = Integer.valueOf(values[0]);
			end_y = Integer.valueOf(values[1]);
			
			maze = new char[row][col];

			for(int i = 3;i < 3+row;i++)
			{
				data = Files.readAllLines(Paths.get(filename)).get(i).replaceAll(" ", "").toCharArray();
				for (int j=0;j<data.length;j++)
				{
					if (data[j] == '1') maze[i-3][j] = '#';
					else maze[i-3][j] = ' ';
				}
			}
			
			maze[end_y][end_x] = 'E';
		}
		catch (FileNotFoundException ex){
	         System.out.println(ex);
	    }
	    catch (IOException ex){
	        System.out.println(ex);
	    }
	}
	
	private boolean FindPath(int x, int y)
	{
		//check if inside maze, at end or not on a path
		if (x < 0 || y < 0 || x >= col || y >=row) return false;
		
		if (maze[y][x] == 'E') return true;

		if (maze[y][x] != ' ' && maze[y][x] != 'S') return false;

		maze[y][x] = '*';
		
		//try in order south east north west
		if (FindPath(x+1, y))
		{
			maze[y][x] = 'X';
			return true;
		}
		if (FindPath(x,y+1))
		{
			maze[y][x] = 'X';
			return true;
		}
		if (FindPath(x-1, y))
		{
			maze[y][x] = 'X';
			return true;
		}
		if (FindPath(x, y-1))
		{
			maze[y][x] = 'X';
			return true;
		}
		
		return false;
	}
	
	private void WriteMaze()
	{
		//mark start
		maze[start_y][start_x] = 'S';
		
		//print all elements and replace * with ' '
		for (int x = 0; x < row;x++)
		{
			for (int y = 0; y < col;y++)
			{
				if (maze[x][y] == '*') maze[x][y] = ' ';
				System.out.print(maze[x][y]);
			}
			System.out.println();
		}
	}
}
